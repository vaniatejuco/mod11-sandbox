#variable
x = 5
print(x)

x = "Hello"
print(x)

x = 123 % 7
print(x)

y,z = 10, 12
print(y)
print(z)

#name, email = "Brandon"
#This will not work

num = 49
name = "Nigoy"
#print(num + name) <- won't work
#print(name + num) <- won't work
#Mini exercise: define 3 variables, num1 to 3 and assig 1, 2.8, 1j to each

num1 = -100 #integer
num2 = 3.0e8 #float
num3 = 3+1j #complex


print(type(num1))
print(type(num2))
print(type(num3))

#type casting
#Number(document. querySelector("#age").value)?
num1 = int(1)
num2 = int(2.8)
num3 = int("3")

print(num1)
print(type(num1))
print(num2)
print(type(num2))
print(num3)
print(type(num3))

name = "Anya"
age = 23

print("Name:" + name +", Age:"+ str(age))

title = "The Hobbit"
print(title[4])
print(len(title))
print(title.upper())

#operators -> arithmetic, comparison, logical(and or not)
a = 12**3
print(str(a))