# a cikkectuib us a group of data
#arrays -> lists

fruits = ["apple", "mango", "kiwi"]
print(fruits)

for fruit in fruits:
	print(fruit)

fruits.append("banana")
print(fruits)

fruits.insert(2, "orange")
print(fruits)


fruits.remove("banana")
print(fruits)

del fruits[2]
print(fruits)

print(len(fruits))

fruits2 = fruits #pass by reference
fruits2 = list(fruits) #correct way of copying
fruits2 = fruits.copy() #correct way of copying
fruits[0] = "mansanas"
print(fruits2)